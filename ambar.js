 

const sqlite3 = require('sqlite3')
const { open } = require('sqlite')
const JSON5 = require('json5')

let _db = {}

function randomInt(max) {
  return Math.floor(Math.random() * max);
}

const db = {
  filename: 'ambar.db3',

  connect: async function() {
    _db = await open({
      filename: this.filename,
      driver: sqlite3.cached.Database
    })
    // const result = await _db.all('SELECT * FROM users')
    // console.log(result)
  },

  parse: function(obj, result, current) {
    for(let key in obj) {
      let value = obj[key]
      if (Array.isArray(value)) continue
      let newKey = (current ? current + "." + key : key)
      if(value && typeof value === "object") {
        this.parse(value, result, newKey)
      } else {
        result[newKey] = value  
      }
    }
  },

  save: async function(obj, table, parent, parentId) {
    let record = {}
    this.parse(obj, record)
    this.saveRecord(record, table, parent, parentId)

    for(let key in obj) {
      let value = obj[key];
      if (Array.isArray(value)) {
        for(let idx in value) {
          let elem = value[idx]
          if(elem && typeof elem === "object") {
            this.save(elem, key, table, record.id)
          }
        }
      } 
    }
    let st = JSON5.stringify(obj, null, 4)
    //console.log(st)
    
  },

  saveRecord: function(record, table, parent, parentId) {
    record.id = randomInt(100)
    if (parent && parent.length)
      record[parent+'Id'] = parentId
    console.log('Saved', table, record)
    return record
  }


  

}

async function run() {
	
}


module.exports = { db }
 