const express = require('express')
const app = express()
const port = 2255

const sqlite3 = require('sqlite3')
const { open } = require('sqlite')
const { db } = require('./ambar')


async function run() {
  let x = {
    name: 'Vasya',
    address: {
      street: "st. Filenina",
      flat: 15,
      city: {
        name: 'SPB',
        index: 195253,
        citytags: ["red", "green"]
      }
    },
    tags: [
      1,2,
      {name: 'anytag', 
        cost: 12, 
        units: [ {eng: 'm', rus: "Ь"} ]
      }
    ],
  }
	db.save(x, 'users')
}


run()


// app.get('/select/:id', function (req, res) {
//   res.send('ok')
// })

// app.listen(port, () => {
//   console.log(`HTTP app listening on port ${port}`)
// })